# Grass Data Explorer

The Grass Data Explorer is a QGIS Plugin to browse and 
visualize raster, vector and time series data.
This Plugin is highly experimental and object of daily changes.

## Installation:

1. Clone the repository:

        git clone https://bitbucket.org/huhabla/grass-data-explorer.git

2. Switch into the Plugin directory and run make to deploy the
   Plugin in the QGIS plugin directory:

        cd grass-data-explorer
        make deploy

Then start QGIS from within a GRASS GIS session.
Enable this Plugin in QGIS Extension manager.

A new GRASS GIS icon should appear in the toolbar. 


## ATTENTION:

The Plugin works only with the latest GRASS GIS 7.1 svn version.

To use this Plugin, QGIS must be started from within an 
active GRASS GIS 7.1 session.

The following Python libraries must be installed:

    * matplotlib with Qt4 support (python-matplotlib-qt4 and maybe python-matplotlib-qt5)
    * numpy
    * PyQt4-devel

On Ubuntu 14.04 make sure that the following packages are installed:

    * python-qt4-dev
    * python-qt4
    * pyqt4-dev-tools
    * python-sphinx
    * python-matplotlib