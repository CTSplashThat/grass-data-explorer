# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassDataExplorer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os.path

grass_import_success = True
# Check if we are in a grass session
try:
    from grass_data_explorer_main_window import GrassDataExplorerMainWindow
    from grass_raster_layer import GrassRasterLayerType
    from grass_strds_layer import GrassSTRDSLayerType
    from grass_stvds_layer import GrassSTVDSLayerType
    from grass_raster_layer import GrassRasterLayer
    from grass_strds_layer import GrassSTRDSLayer
    from grass_stvds_layer import GrassSTVDSLayer
except ImportError, e:
    # We are not in a GRASS session
    grass_import_success = False
    print(e)


class GrassDataExplorer:
    """Main class to setup the plugin and loaf the initial GUI
    """

    # Set the class variable if we are in a grass session
    global grass_import_success
    in_grass_session = grass_import_success

    def __init__(self, iface):
        """Konstructor"""
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        self.plugin_main = None
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n',
                                  'grass_data_explorer{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        self.grass_iface = None
        self.grass_data_provider = None

    def __exit__(self, *args):
        if self.in_grass_session is True:
            if self.plugin_main:
                self.plugin_main.clean_up()
                self.plugin_main.close()
                self.plugin_main = None

    def __del__(self, *args):
        if self.in_grass_session is True:
            if self.plugin_main:
                self.plugin_main.clean_up()
                self.plugin_main.close()
                self.plugin_main = None

    def initGui(self):
        if self.in_grass_session is True:
            # Create action that will start plugin configuration
            icon = QIcon()
            icon.addPixmap(QPixmap(":/icons/grass_logo.svg"), QIcon.Normal, QIcon.Off)
            self.action = QAction(icon, u"Grass Data Explorer", self.iface.mainWindow())
            # connect the action to the run method
            self.action.triggered.connect(self.run)

            # Add toolbar button and menu item
            self.iface.addToolBarIcon(self.action)
            self.iface.addPluginToMenu(u"&Grass Data Explorer", self.action)

            # Register the plugin layer
            self.pluginTypeRaster = GrassRasterLayerType()
            QgsPluginLayerRegistry.instance().addPluginLayerType(self.pluginTypeRaster)
            self.pluginTypeSTRDS = GrassSTRDSLayerType()
            QgsPluginLayerRegistry.instance().addPluginLayerType(self.pluginTypeSTRDS)
            self.pluginTypeSTVDS = GrassSTVDSLayerType()
            QgsPluginLayerRegistry.instance().addPluginLayerType(self.pluginTypeSTVDS)

            # Create the the dialog
            self.plugin_main = GrassDataExplorerMainWindow(self.iface)

    def unload(self):
        if self.in_grass_session is True:
            # Remove the plugin menu item and icon
            self.iface.removePluginMenu(u"&Grass Data Explorer", self.action)
            self.iface.removeToolBarIcon(self.action)

            QgsPluginLayerRegistry.instance().removePluginLayerType(GrassRasterLayer.LAYER_TYPE)
            self.pluginTypeSTRDS = GrassSTRDSLayerType()
            QgsPluginLayerRegistry.instance().removePluginLayerType(GrassSTRDSLayer.LAYER_TYPE)

            # Close the dialog
            self.plugin_main.clean_up()
            self.plugin_main.close()
            self.plugin_main = None

    # run method that performs all the real work
    def run(self):
        if self.in_grass_session is True:
            # Load all data
            self.plugin_main.load_all()
            # show the dialog
            self.plugin_main.show()
            # Run the dialog event loop
            #result = self.plugin_main.exec_()
            # See if OK was pressed
            #if result == 1:
                # do something useful (delete the line containing pass and
                # substitute with your code)
            #    pass
