# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassRasterLayer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from raster_settings_dialog import RasterSettingsDialog
from grass.pygrass.rpc import DataProvider
from grass.temporal import CLibrariesInterface
import grass.script as gscript


################################################################################


class GrassRasterLayer(QgsPluginLayer):

    LAYER_TYPE = "GrassRasterLayer"

    def __init__(self, layer_type=None):
        if layer_type is None:
            layer_type = self.LAYER_TYPE
        QgsPluginLayer.__init__(self, layer_type)
        self.setValid(True)
        self.opacity = 1.0
        self.mapUnits = 0

        # Each layer has its own grass processes for data generation
        self.grass_data_provider = DataProvider()
        self.grass_iface = CLibrariesInterface()
        self.dlg = None

    def stop_grass(self):
        #QgsMessageLog.logMessage("GrassRasterLayer: stop_grass: Stop GRASSPluginLayer provider processes")
        self.grass_data_provider.stop()
        self.grass_iface.stop()

    def clean_up(self):
        self.stop_grass()
        if self.dlg:
            self.dlg.clean_up()
            self.dlg.close()
            self.dlg = None

    def __exit__(self):
        # Make sure the server subprocess are killed
        self.clean_up()

    def __del__(self):
        # Make sure the server subprocess are killed
        self.clean_up()

    def setOpacity(self, opacity):
        self.opacity = opacity

    def raster_exist(self):
        """
        Check if the raster layer exists
        :return: True is exists, False otherwise
        """
        if not self.raster_name or not self.mapset:
            return False

        return bool(self.grass_iface.raster_map_exists(self.raster_name, self.mapset))

    def initialize(self, raster_name, mapset, info_text, iface):

        self.raster_name = raster_name
        self.mapset = mapset
        self.info_text = info_text
        self.iface = iface

        if self.raster_exist() is True:
            #QgsMessageLog.logMessage("GrassRasterLayer: initialize: Read GRASS raster %s from mapset %s" %
            #                         (self.raster_name, self.mapset))
            self.info = self.grass_iface.read_raster_info(self.raster_name,
                                                          self.mapset)

            self.dlg = RasterSettingsDialog(iface=iface,
                                        raster_layer=self)

            self.setLayerName(self.raster_name)

            wkt_proj = gscript.read_command("g.proj", flags="w")

            crs = QgsCoordinateReferenceSystem(wkt_proj)
            self.mapUnits = crs.mapUnits()

            self.setCrs(crs)

            # We set the extent from the raster map layer
            self.setExtent(QgsRectangle(self.info["west"], self.info["south"],
                                        self.info["east"], self.info["north"]))

            self.repaintRequested.emit()
            return True
        else:
            QgsMessageLog.logMessage("GrassRasterLayer: initialize: Raster map layer does not exist")
            return False

    def legendSymbologyItems(self, iconSize):
        if self.raster_exist() is True:

            colors = gscript.parse_command("r.colors.out", map=self.raster_name)

            legend = {}
            for line in colors:
                name, color = line.split()[0:2]
                try:
                    name = int(name)
                except:
                    try:
                        name = float(name)
                    except:
                        pass
                legend[name] = color

            sorted_keys = sorted(legend)

            l = []
            for name in sorted_keys:
                color = legend[name]
                p = QPixmap(iconSize)
                r, g, b = color.split(":")
                p.fill(QColor(int(r), int(g), int(b)))
                l.append((str(name), p))

            return l
        return []

    def set_opacity(self, opacity):
        """Set the opacity of the layer

           :param opacity: A value between 0 and 100
        """
        if self.raster_exist() is True:
            self.opacity = opacity / 100.0
            self.repaintRequested.emit()

    def showRasterSettings(self):
        if self.dlg is not None:
            self.dlg.show()
            # Run the dialog event loop
            result = self.dlg.exec_()
            # See if OK was pressed
            if result == 1:
                # do something useful (delete the line containing pass and
                # substitute with your code)
                pass

    def createMapRenderer(self, context):
        return GrassRasterLayersRenderer(self, context)


################################################################################


class GrassRasterLayersRenderer(QgsMapLayerRenderer):
    def __init__(self, layer, context):
        QgsMapLayerRenderer.__init__(self, layer.id())
        self.rendererContext = context
        self.layer = layer
        self.mutex = QMutex()

    def render(self):
        """
        Render a GRASS GIS raster layer.

        This function calls get_raster_image_as_np()
        from the PyGRASS rpc library that returns a numpy array with 32Bit color
        information of the raster map layer using the current QGIS mapCanvas extent.
        This array is then copied into a QImage and drawn by the painter.
        :return:
        """
        painter = self.rendererContext.painter()
        if self.layer.raster_exist() is True:
            extent = self.rendererContext.extent()
            rasterScaleFactor = self.rendererContext.rasterScaleFactor()
            invRasterScaleFactor = 1.0 / rasterScaleFactor
            west = extent.xMinimum()
            east = extent.xMaximum()
            south = extent.yMinimum()
            north = extent.yMaximum()

            #QgsMessageLog.logMessage("GrassRasterLayer: North %f South %f"%(north, south))

            # Get rows and cols
            mapToPixel = self.rendererContext.mapToPixel()
            upper_right_orig = mapToPixel.transform(east, north)

            # Adjust the extent in case of degrees to read only valid data
            if self.layer.mapUnits == 2:
                if north > 90:
                    north = 90

                if north < 0:
                    north = 0

                if south < -90:
                    south = -90

                if south > 90:
                    south = 90

            lower_left = mapToPixel.transform(west, south)
            upper_right = mapToPixel.transform(east, north)

            rows = upper_right.y() - lower_left.y()
            cols = upper_right.x() - lower_left.x()

            offset_x = int(upper_right.x() - upper_right_orig.x())
            offset_y = int(upper_right.y() - upper_right_orig.y())

            #QgsMessageLog.logMessage("GrassRasterLayer: Offset x " + str(offset_x))
            #QgsMessageLog.logMessage("GrassRasterLayer: Offset y " + str(offset_y))

            rows *= rasterScaleFactor
            cols *= rasterScaleFactor
            rows = int(abs(rows))
            cols = int(abs(cols))

            extent = {"north": north, "south": south, "east": east,
                      "west": west, "rows": rows, "cols": cols}

            #QgsMessageLog.logMessage("GrassRasterLayer: Extent %s"%(str(extent)))

            if cols <= 0 or rows <= 0:
                return False

            self.mutex.lock()
            array = self.layer.grass_data_provider.get_raster_image_as_np(self.layer.raster_name,
                                                                          self.layer.mapset,
                                                                          extent,
                                                                          color="RGB")
            self.mutex.unlock()

            if array is not None:
                self.image = QImage(array.data, cols, rows,
                                    QImage.Format_RGB32)

                painter.save()
                painter.setOpacity(self.layer.opacity)
                painter.scale(invRasterScaleFactor, invRasterScaleFactor)
                # Set the correct offset
                painter.drawImage(offset_x, offset_y, self.image)
                painter.restore()
                return True
            else:
                return False
        else:
            QgsMessageLog.logMessage("Raster layer <%s> does not exist"%(self.layer.raster_name))
            return False

################################################################################


class GrassRasterLayerType(QgsPluginLayerType):
    def __init__(self):
        QgsPluginLayerType.__init__(self, GrassRasterLayer.LAYER_TYPE)

    def createLayer(self):
        return GrassRasterLayer()

    def showLayerProperties(self, layer):
        layer.showRasterSettings()
        return True

