# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassSampleTool
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.gui import *
from qgis.core import *
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QMessageBox
import datetime
import grass.script as gscript
import csv

class GrassSampleTool(QgsMapTool):
    """
    A simple coordinate gather tool
    that provides methods to sample GRASS raster map layers and STRDS
    """
    # The signal that will be emitted by this tool
    canvasClicked = pyqtSignal(QgsPoint)

    def __init__(self, canvas):
        """
           :param canvas: MapCanvas reference
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.sample_history = []
        self.sample_counter = 0
        self.sample_history_size = 1000

    def canvasReleaseEvent(self, event):
        """Release event method from super class
           :param event: QMouseEvent
        """
        x = event.pos().x()
        y = event.pos().y()

        tranformer = self.canvas.getCoordinateTransform()
        point = tranformer.toMapCoordinates(x, y)

        # Emit the signal and deliver the point
        self.canvasClicked.emit(point)

    def sample_vectors_by_point(self,
                                point,
                                distance,
                                table_id,
                                vector_names):
        """
        Call v.what to sample a stack of vector map layers with a single point.

        :param point: QgsPoint that contains the coordinates in the GRASS location projection
        :param vector_names: A list of vector map names
        :return: A text string containing up to 50 samples with the latest sample result at the top
        """
        self.sample_counter += 1

        # Run r.what using the raster region settings
        coords = "%f,%f"%(point.x(), point.y())

        try:
            output = gscript.read_command("v.what",
                                          coordinates=coords,
                                          map=",".join(vector_names),
                                          distance=distance,
                                          layer=table_id,
                                          flags="a")
        except Exception, e:
            QMessageBox.critical(None, "Error", "Error in r.what:\n%s"%(str(e)))
            return None

        content_line = "Sample %06i\n" \
                       "  Date:............ %s\n"%(
                       self.sample_counter,
                       str(datetime.datetime.now()))

        lines = output.split("\n")

        for line in lines:
            content_line += "  %s\n"%line

        content_line += "\n"

        self.sample_history.append(content_line)
        self.sample_history.reverse()

        # Generate the text representation
        result = "\n".join(self.sample_history)

        if len(self.sample_history) > self.sample_history_size:
            del self.sample_history[self.sample_history_size:]
        self.sample_history.reverse()

        return result

    def sample_rasters_by_point(self,
                                point,
                                raster_names):
        """
        Call r.what to sample a stack of raster map layers with a single point.
        50 sample results will be held in memory as text.

        :param point: QgsPoint that contains the coordinates in the GRASS location projection
        :param raster_names: A list of raster map names
        :return: A text string containing up to 50 samples with the latest sample result at the top
        """
        self.sample_counter += 1

        # Run r.what using the raster region settings
        coords = "%f,%f"%(point.x(), point.y())
        gscript.use_temp_region()

        if len(raster_names) == 1:
            try:
                gscript.run_command("g.region", raster=raster_names[0], flags="p")
            except Exception, e:
                QMessageBox.critical(None, "Error", "Error in g.region :\n%s"%(str(e)))
                gscript.del_temp_region()
                return None

        try:
            output = gscript.read_command("r.what",
                                          coordinates=coords,
                                          map=",".join(raster_names),
                                          flags="fr")
        except Exception, e:
            QMessageBox.critical(None, "Error", "Error in r.what:\n%s"%(str(e)))
            gscript.del_temp_region()
            return None

        gscript.del_temp_region()

        content_line = "Sample %06i\n" \
                       "  Date:............ %s\n" \
                       "  Coordinates:..... %f,%f\n"%(
                       self.sample_counter,
                       str(datetime.datetime.now()),
                       point.x(),
                       point.y())

        start = 3
        for raster_name in raster_names:
            end = start + 3

            value, cat, rgb = output.split("|")[start:end]

            content_line += "  Map name:........ %s\n" \
                            "    Value:......... %s\n" \
                            "    Category:...... %s\n" \
                            "    Color:......... %s\n"%(
                            raster_name,
                            str(value).strip(),
                            str(cat).strip(),
                            str(rgb).strip())
            start += 3

        self.sample_history.append(content_line)
        self.sample_history.reverse()

        # Generate the text representation
        result = "\n".join(self.sample_history)

        if len(self.sample_history) > self.sample_history_size:
            del self.sample_history[self.sample_history_size:]
        self.sample_history.reverse()

        return result

    def sample_strds_by_point(self,
                              point,
                              where,
                              strds):

        """
        Call t.rast.sample to sample a space time raster dataset with a single point.

        :param point: QgsPoint that contains the coordinates in the GRASS location projection
        :param where: A where stament to pre-select the maps that should be used for sampling
        :param strds: The GrassSTRDSLayer object
        :return: A tuple:

                 1. A dictionary that contains lists of sample values, start-time, end-time as strings and
                    datetime objects and the position in seconds for trend computation

                 2. The name of the temporary text file with the sample result
        """
        self.sample_counter += 1

        # Run t.rast.sample using the raster region settings
        coords = "%f,%f"%(point.x(), point.y())
        temp_file = gscript.tempfile(False)
        null_value = "nan"

        try:
            gscript.read_command("t.rast.sample",
                                 where=where,
                                 coordinates=coords,
                                 output=temp_file,
                                 strds=strds.get_id(),
                                 flags="r",
                                 overwrite=True)
        except Exception, e:
            QMessageBox.critical(None, "Error", "Error in t.rast.what:\n%s"%(str(e)))
            return None

        r = csv.reader(open(temp_file, "r"), delimiter="|")

        values = []
        pos = []
        starts = []
        ends = []
        starts_strings = []
        ends_strings = []

        first = True
        for e in r:
            print e
            # Jump over NULL values
            if e[2] is None or e[2] == null_value or "None" in e[2]:
                continue
            else:
                values.append(float(e[2]))

            if strds.get_temporal_type() == "absolute":
                if first is True:
                    first = datetime.datetime.strptime(e[0].split(".")[0], "%Y-%m-%d %H:%M:%S")
                start = datetime.datetime.strptime(e[0].split(".")[0], "%Y-%m-%d %H:%M:%S")
                end = e[1]
                if end is not None and end != 'None':
                    end = datetime.datetime.strptime(e[1].split(".")[0], "%Y-%m-%d %H:%M:%S")
                delta = start - first
                pos.append(delta.total_seconds())
            else:
                start = int(e[0])
                end = int(e[1])
                pos.append(start)

            starts.append(start)
            starts_strings.append(str(start))
            ends_strings.append(str(end))
            ends.append(end)

        result = {}
        if len(values) == 1:
            result["values"] = []
            result["starts"] = []
            result["ends"] = []
            result["starts_strings"] = []
            result["ends_strings"] = []
            result["pos"] = []
        else:
            result["values"] = values
            result["starts"] = starts
            result["ends"] = ends
            result["starts_strings"] = starts_strings
            result["ends_strings"] = ends_strings
            result["pos"] = pos

        return result, temp_file
