# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassSTVDSLayer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import sys
from PyQt4.QtCore import *
from qgis.core import *
from grass.pygrass.rpc import DataProvider
from grass.temporal import CLibrariesInterface
import grass.script as gscript
from grass_vector_to_memory_layer import grass_vector_to_memory_layer
from stvds_settings_dialog import STVDSSettingsDialog


################################################################################


class GrassSTVDSLayer(QgsPluginLayer):

    LAYER_TYPE = "GrassSTVDSLayer"

    # The signal that will be emitted in case a vector
    # layer was loaded
    vectorCacheChanged = pyqtSignal()

    def __init__(self, layer_type=None):
        if layer_type is None:
            layer_type = self.LAYER_TYPE
        QgsPluginLayer.__init__(self, layer_type)
        self.setValid(True)
        self.vector_exist = False
        self.opacity = 1.0
        self.mapUnits = 0
        self.grass_data_provider = DataProvider()
        self.grass_iface = CLibrariesInterface()
        self.feat_type = None
        self.add_att = None
        self.bbox = None
        self.vector_name = None
        self.mapset = None
        self.info_text = None
        self.iface = None
        self.layer_style_file = None
        self.label_style_file = None
        self.layer_cache = {}
        self.layer_name_list = []
        self.mutex = QMutex()
        self.max_layer_cache_size = 50

    def load_styles(self):
        """
        Load layer and label styles to the current vector layer
        :return:
        """
        if self.layer_style_file is not None and self.layer_style_file != "":
            if self.vector_name in self.layer_cache:
                self.layer_cache[self.vector_name].loadNamedStyle(self.layer_style_file)

        if self.label_style_file is not None and self.label_style_file != "":
            if self.vector_name in self.layer_cache:
                self.layer_cache[self.vector_name].loadNamedStyle(self.label_style_file)

        self.propagate_changes()

    def propagate_changes(self):
        """
        Emit all needed signals to redraw the map canvas and legend
        :return:
        """
        # Emit the change signals
        self.layerNameChanged.emit()
        self.legendChanged.emit()
        self.repaintRequested.emit()

    def set_layer_style_file(self, file_name):
        self.layer_style_file = file_name
        self.load_styles()

    def set_label_style_file(self, file_name):
        self.label_style_file = file_name
        self.load_styles()

    def stop_grass(self):
        message = "GrassSTVDSLayer: stop_grass: Stop GRASSPluginLayer provider processes"
        #QgsMessageLog.logMessage(message)
        sys.stderr.write(message + "\n")
        self.grass_iface.stop()
        self.grass_data_provider.stop()

    def clear_layer_cache(self):
        self.mutex.lock()
        for layer in self.layer_cache.values():
            message = "GrassSTVDSLayer: clear_layer_cache: Delete memory layer %s"%layer.id()
            #QgsMessageLog.logMessage(message)
            sys.stderr.write(message + "\n")
            del layer
        self.layer_cache.clear()
        self.layer_name_list = []
        self.mutex.unlock()
        self.vectorCacheChanged.emit()
        
    def set_max_layer_cache_size(self, size):
        self.clear_layer_cache()
        self.max_layer_cache_size = size
        self.vectorCacheChanged.emit()

    def clean_up(self):
        self.clear_layer_cache()
        # Make sure the server subprocess are killed
        self.stop_grass()
        # Close the GUI
        if self.dlg:
            self.dlg.close()
            self.dlg = None

    def __exit__(self):
        self.clean_up()

    def __del__(self):
        self.clean_up()

    def _set_layer_name(self):
        self.setLayerName("STVDS:  " + self.stvds_name + "\n  map:  " +
                          self.vector_name)

    def initialize(self,
                   vector_name,
                   mapset,
                   info_text,
                   iface,
                   stvds_name,
                   feat_type,
                   add_attr=True,
                   bbox=None):

        self.vector_name = vector_name
        self.stvds_name = stvds_name
        self.mapset = mapset
        self.info_text = info_text
        self.iface = iface
        self.feat_type = feat_type
        self.add_attr = add_attr
        self.bbox = bbox

        self.dlg = STVDSSettingsDialog(iface=iface,
                                       stvds_layer=self,
                                       feat_type=feat_type,
                                       add_attr=add_attr,
                                       bbox=bbox)
        message = "GrassSTVDSLayer: initialize: Read vector map %s from mapset %s" %(self.vector_name, self.mapset)
        #QgsMessageLog.logMessage(message)
        sys.stderr.write(message + "\n")
        self.info = self.grass_iface.read_vector_info(self.vector_name,
                                                      self.mapset)
        self.vector_exist = False
        if self.info:
            self.vector_exist = True

        if self.vector_exist is True:

            self._set_layer_name()

            wkt_proj = gscript.read_command("g.proj", flags="w")

            crs = QgsCoordinateReferenceSystem(wkt_proj)
            self.mapUnits = crs.mapUnits()

            self.setCrs(crs)

            # We set the extent from the raster map layer
            self.setExtent(QgsRectangle(self.info["west"], self.info["south"],
                                        self.info["east"], self.info["north"]))

            if self.vector_name not in self.layer_cache.keys():
                self.layer_cache[self.vector_name] = grass_vector_to_memory_layer(self.grass_data_provider,
                                                                                  self.grass_iface,
                                                                                  self.iface,
                                                                                  self.vector_name,
                                                                                  self.mapset,
                                                                                  self.feat_type,
                                                                                  self.add_attr,
                                                                                  self.bbox)
                self.layer_name_list.append(self.vector_name)
                self.vectorCacheChanged.emit()

            self.load_styles()
            self.vectorCacheChanged.emit()
            return True
        else:
            QgsMessageLog.logMessage("GrassSTVDSLayer: initialize: Raster map layer does not exist")
            return False

    def change_map(self,
                   vector_name,
                   feat_type,
                   add_attr=True,
                   bbox=None):

        self.vector_name = vector_name
        self.feat_type = feat_type
        self.add_attr = add_attr
        self.bbox = bbox

        self.info = self.grass_iface.read_vector_info(self.vector_name,
                                                      self.mapset)

        self.vector_exist = False
        if self.info:
            self.vector_exist = True

            self._set_layer_name()

            # We set the extent from the vector map layer if no bbox was specified
            self.setExtent(QgsRectangle(self.info["west"],
                                        self.info["south"],
                                        self.info["east"],
                                        self.info["north"]))

            if self.vector_name not in self.layer_cache.keys():
                QgsMessageLog.logMessage("Vector %s not in layerstack"%self.vector_name)
                self.layer_cache[self.vector_name] = grass_vector_to_memory_layer(self.grass_data_provider,
                                                                                  self.grass_iface,
                                                                                  self.iface,
                                                                                  self.vector_name,
                                                                                  self.mapset,
                                                                                  self.feat_type,
                                                                                  self.add_attr,
                                                                                  self.bbox)
                self.layer_name_list.append(self.vector_name)
                self.vectorCacheChanged.emit()

            # Remove the first layer in the layer stack
            if len(self.layer_name_list) > self.max_layer_cache_size:
                QgsMessageLog.logMessage("Layerstack is full, remove vector layer %s"%self.layer_name_list[0])
                self.layer_cache.pop(self.layer_name_list[0])
                self.layer_name_list.pop(0)
                self.vectorCacheChanged.emit()

            self.load_styles()
            return True
        else:
            QgsMessageLog.logMessage("GrassSTVDSLayer: change_map: Raster map layer does not exist")
            return False

    def showSTVDSSettings(self):
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass

    def currentVectorLayer(self):
        """
        Return the currently rendered vector layer
        :return:
        """
        if self.vector_exist is True and self.vector_name in self.layer_cache:
            QgsMessageLog.logMessage("GrassSTVDSLayer: currentVectorLayer: Return current layer")
            return self.layer_cache[self.vector_name]
        else:
            return None

    def legendSymbologyItems(self, iconSize):
        if self.vector_exist is True and self.vector_name in self.layer_cache:
            QgsMessageLog.logMessage("GrassSTVDSLayer: legendSymbologyItems: Creating symbology items")
            return self.layer_cache[self.vector_name].rendererV2().legendSymbologyItems(iconSize)

    def createMapRenderer(self, context):
        if self.vector_exist is True and self.vector_name in self.layer_cache:
            #QgsMessageLog.logMessage("GrassSTVDSLayer: createMapRenderer: Creating renderer")
            return self.layer_cache[self.vector_name].createMapRenderer(context)
        return None


################################################################################


class GrassSTVDSLayerType(QgsPluginLayerType):
    def __init__(self):
        QgsPluginLayerType.__init__(self, GrassSTVDSLayer.LAYER_TYPE)

    def createLayer(self):
        return GrassSTVDSLayer()

    def showLayerProperties(self, layer):
        layer.showSTVDSSettings()
        return True

