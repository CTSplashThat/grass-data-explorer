# -*- coding: utf-8 -*-
"""
/***************************************************************************
 STRDSSettingsDialog
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from qgis.core import *
from ui_strds_settings import Ui_STRDSSettings
import grass.script as gscript
import grass.temporal as tgis
import csv

# matplotlib stuff
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import numpy as np

from grass_sample_tool import GrassSampleTool
from time_series_stats import TimeSeriesStatisticsComputation

# create the dialog for zoom to point
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class STRDSSettingsDialog(QtGui.QDialog, Ui_STRDSSettings):
    """
    This class manages all settings and analysis that can be
    performed on space time raster datasets.
    """
    def __init__(self, iface, strds_layer):
        QtGui.QDialog.__init__(self)

        self.setupUi(self)
        self.iface = iface
        self.strds_layer = strds_layer

        self.map_name_dict = {}
        self.canvas = self.iface.mapCanvas()

        # Add layer name to window title
        title = self.windowTitle()
        self.setWindowTitle(title + " (%s)"%(self.strds_layer.strds_name))

        # Setup database connection
        self.dbif, self.connected = tgis.init_dbif(None)
        # Load the strds
        self.strds = tgis.open_old_stds(self.strds_layer.strds_name,
                                        "strds", self.dbif)
        self.strds.select(self.dbif)
        # Generate the maplist
        self.map_list = self.strds.get_registered_maps(columns="name,start_"
                                                               "time,end_"
                                                               "time,min, "
                                                               "max",
                                                       order="start_time",
                                                       dbif=self.dbif)
        if self.connected is True:
            self.dbif.close()

        self.plainTextEdit.setPlainText(self.strds_layer.info_text)
        self.rasterNameLabel.setText("Current map:   " +
                                     self.strds_layer.raster_name)

        # Setup the toolbar
        self.toolBar = QtGui.QToolBar()
        self.verticalLayout.insertWidget(0, self.toolBar)

        # Connect buttons
        self.treeWidget.itemClicked.connect(self.switch_raster_layer)
        self.treeWidget.itemActivated.connect(self.switch_raster_layer)
        self.treeWidget.currentItemChanged.connect(self.switch_raster_layer)
        self.strds_layer.layerNameChanged.connect(self.update_map_list_selection)

        self.pushButtonOpacity.clicked.connect(self.set_opacity)
        self.pushButtonRasterStats.clicked.connect(self.generate_raster_history_plot)
        self.pushButtonGenerateTimeStats.clicked.connect(self.generate_timeseries_stats_and_plot)
        self.pushButtonListMaps.clicked.connect(self.generate_show_map_list)

        self.pushButtonAbortTimeSeriesStats.clicked.connect(self.kill_time_stats_process)

        self.pushButtonApplyColors.clicked.connect(self.apply_colors)

        # GRASS raster QgsMapTool
        self.grass_raster_sample_tool = GrassSampleTool(self.canvas)
        self.grass_raster_sample_tool.canvasClicked.connect(self.sample_raster_point)

        # GRASS strds QgsMapTool
        self.grass_strds_sample_tool = GrassSampleTool(self.canvas)
        self.grass_strds_sample_tool.canvasClicked.connect(self.sample_strds_point)

        # Create action for the raster map samppling
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/SampleRaster.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.rasterSampleAction = QtGui.QAction(icon, u"Sample current Grass Raster Layer", self)
        # connect the action to the run method
        self.rasterSampleAction.triggered.connect(self.set_grass_raster_sample_tool)

        # Add toolbar button and menu item
        self.toolBar.addAction(self.rasterSampleAction)

        # Create action for the raster map samppling
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/SampleSTRDS.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.strdsSampleAction = QtGui.QAction(icon, u"Sample Grass STRDS Layer", self)
        # connect the action to the run method
        self.strdsSampleAction.triggered.connect(self.set_grass_strds_sample_tool)

        # Add toolbar button and menu item
        self.toolBar.addAction(self.strdsSampleAction)

        self.show_map_list()
        self.setup_raster_plot()
        self.setup_timeseries_plots()

        self.objThread = QtCore.QThread()
        self.time_stats = TimeSeriesStatisticsComputation(self.strds, self)
        self.mutex = QtCore.QMutex()

        self.listWidget.itemClicked.connect(self.switch_stacked_widget)
        self.listWidget.itemActivated.connect(self.switch_stacked_widget)
        self.listWidget.currentItemChanged.connect(self.switch_stacked_widget)

    def switch_stacked_widget(self):
        self.stackedWidget.setCurrentIndex(self.listWidget.currentIndex().row())

        if self.listWidget.currentIndex().row() == 4:
            self.set_grass_raster_sample_tool()
        elif self.listWidget.currentIndex().row() == 6:
            self.set_grass_strds_sample_tool()

    def __del__(self):
        self.objThread.quit()
        self.objThread.deleteLater()

    def __exit__(self):
        self.objThread.quit()
        self.objThread.deleteLater()

    def clean_up(self):
        self.canvas.unsetMapTool(self.grass_raster_sample_tool)
        self.canvas.unsetMapTool(self.grass_strds_sample_tool)

    def check_raster_layer(self):

        if self.strds_layer.raster_exist() is False:
            QtGui.QMessageBox.critical(self, "Error",
                                       "GRASS raster layer <%s> does not exist"%(self.strds_layer.raster_name))
            return False

        return True

    def setup_timeseries_plots(self):
        """
        Set up the matplotlib figures and canvas for plotting
        the time series statistics and time series sampling
        :return:
        """

        # Statistical plots
        self.figureMin = plt.figure()
        self.figureMax = plt.figure()
        self.figureMean = plt.figure()
        self.figureStddev = plt.figure()
        self.figureVarianz = plt.figure()

        self.plotCanvasMin = FigureCanvas(self.figureMin)
        self.plotCanvasMax = FigureCanvas(self.figureMax)
        self.plotCanvasMean = FigureCanvas(self.figureMean)
        self.plotCanvasStddev = FigureCanvas(self.figureStddev)
        self.plotCanvasVarianz = FigureCanvas(self.figureVarianz)

        self.vLayoutMin.addWidget(self.plotCanvasMin)
        self.vLayoutMax.addWidget(self.plotCanvasMax)
        self.vLayoutMean.addWidget(self.plotCanvasMean)
        self.vLayoutStddev.addWidget(self.plotCanvasStddev)
        self.vLayoutVarianz.addWidget(self.plotCanvasVarianz)

        # Sampling plot
        self.figureSTRDSSampling = plt.figure()
        self.plotCanvasSTRDSSampling = FigureCanvas(self.figureSTRDSSampling)
        self.vLayoutSTRDSSampling.addWidget(self.plotCanvasSTRDSSampling)

    @QtCore.pyqtSlot()
    def set_grass_raster_sample_tool(self):
        """
        Set the grass sample MapTool and switch to the
        output tab.
        :return:
        """
        if self.check_raster_layer() is True:
            self.stackedWidget.setCurrentIndex(4)
            self.canvas.setMapTool(self.grass_raster_sample_tool)

    @QtCore.pyqtSlot(QgsPoint)
    def sample_raster_point(self, point):
        """
        Call the raster sample method from the grass sample tool
        and switch to the output tab.
        :param point: QgsPoint of coordinates in the GRASS location coordinate system
        :return:
        """
        if self.check_raster_layer() is True:
            self.stackedWidget.setCurrentIndex(4)
            line = self.grass_raster_sample_tool.sample_rasters_by_point(point,
                                                                         [self.strds_layer.raster_name,])
            if line:
                self.textEditRasterSampling.setText(line)
            else:
                self.textEditRasterSampling.clear()

    @QtCore.pyqtSlot()
    def set_grass_strds_sample_tool(self):
        """
        Set the grass sample MapTool and switch to the
        output tab.
        :return:
        """
        self.stackedWidget.setCurrentIndex(6)
        self.canvas.setMapTool(self.grass_strds_sample_tool)

    @QtCore.pyqtSlot(QgsPoint)
    def sample_strds_point(self, point):
        """
        Call the strds sample method from the grass sample tool
        and switch to the output tab.
        :param point: QgsPoint of coordinates in the GRASS location coordinate system
        :return:
        """
        self.stackedWidget.setCurrentIndex(6)
        self.tableWidgetSTRDSSample.clear()
        self.lineEditSourceFileSTRDS.clear()
        self.figureSTRDSSampling.clear()
        self.plotCanvasSTRDSSampling.draw()

        where = self.lineEditWhereSTRDSSampling.text()
        result = self.grass_strds_sample_tool.sample_strds_by_point(point,
                                                                    where,
                                                                    self.strds)
        coords = "Coordinates (x,y): (%f, %f)"%(point.x(), point.y())
        self.lineEditSampleCoordinates.setText(coords)

        if result:
            data, source_file = result
        else:
            return

        # Set the source file path
        self.lineEditSourceFileSTRDS.setText(source_file)

        if data:
            # Fill the table with data
            labels = ["Start", "End", "Value"]
            columns = ["starts_strings", "ends_strings", "values"]

            self.fill_table_widget(self.tableWidgetSTRDSSample,
                                   3, len(data["values"]),
                                   data, labels, columns)
            # Plot the data
            self.generate_timeseries_plot(data["values"],
                                          data["pos"],
                                          data["starts"],
                                          "sampled",
                                          self.figureSTRDSSampling,
                                          self.plotCanvasSTRDSSampling)

    @staticmethod
    def fill_table_widget(table,
                          num_cols,
                          num_rows,
                          data,
                          labels,
                          columns):
        """
        Fill a table widget with generic data
        :param table: The table widget to fill
        :param num_cols: The number of columns
        :param num_rows: The number of rows
        :param data: The data organised as lists in a dictionary {"value":[1,2,3], "dates":[1,2,3], ...}
        :param labels: A list with label names
        :param columns: The column numbers in the data dictionary, columns and labels should have the same order
        :return:
        """
        table.setRowCount(num_rows)
        table.setColumnCount(num_cols)
        table.setHorizontalHeaderLabels(labels)

        for column in columns:
            index = columns.index(column)
            count = 0
            for value in data[column]:
                new_item = QtGui.QTableWidgetItem()
                new_item.setData(QtCore.Qt.DisplayRole, value)
                table.setItem(count, index, new_item)
                count += 1

        for index in range(len(columns)):
            table.resizeColumnToContents(index)

    @QtCore.pyqtSlot(dict, str)
    def generate_timeseries_plots(self, data, source_file):
        """
        Slot to create the time series statistics plots
        :param data:
        :return:
        """

        # Set the source file path
        self.lineEditSourceFile.setText(source_file)

        self.tableWidgetRawData.clear()
        if data:
            # Fill the table with data
            labels = ["Dates", "Minimum", "Maximum", "Mean", "Std. Deviation", "Variance"]
            columns = ["date_strings", "min", "max", "mean", "stddev", "variance"]

            self.fill_table_widget(self.tableWidgetRawData,
                                   6, len(data["pos"]),
                                   data, labels, columns)

            # Generate the plots
            self.generate_timeseries_plot(data["min"], data["pos"],
                                          data["dates"], "Minimum",
                                          self.figureMin,
                                          self.plotCanvasMin)

            self.generate_timeseries_plot(data["max"], data["pos"],
                                          data["dates"], "Maximum",
                                          self.figureMax,
                                          self.plotCanvasMax)

            self.generate_timeseries_plot(data["mean"], data["pos"],
                                          data["dates"], "Mean",
                                          self.figureMean,
                                          self.plotCanvasMean)

            self.generate_timeseries_plot(data["stddev"], data["pos"],
                                          data["dates"], "Standard deviation",
                                          self.figureStddev,
                                          self.plotCanvasStddev)

            self.generate_timeseries_plot(data["variance"], data["pos"],
                                          data["dates"], "Varianz",
                                          self.figureVarianz,
                                          self.plotCanvasVarianz)

        self._reset_generate_time_stats_button()

    @staticmethod
    def generate_timeseries_plot(data, pos, dates, name, figure,
                                 plotCanvas):
        """Generate a time series plot

           :param data: A list of values (y-axes)
           :param pos: List of numerical position for polygonal fit
           :param dates: A list of dates (x-axes)
           :param name: The name of the data
           :param figure: The figure used for plotting
           :param plotCanvas: The canvas which should be drawn
        """
        figure.clear()

        if len(data) > 0:
            # Generate the data to plot
            try:
                ax1 = figure.add_subplot(211)
                ax1_fit1 = np.polyfit(pos, data, 1)
                ax1_fit2 = np.polyfit(pos, data, 5)
                ax1_fit_fn1 = np.poly1d(ax1_fit1)
                ax1_fit_fn2 = np.poly1d(ax1_fit2)
                ax1.plot(dates, data, 'r.',
                         dates, ax1_fit_fn1(pos), '--k',
                         dates, ax1_fit_fn2(pos), '--b')
                ax2 = figure.add_subplot(212)
                ax2.hist(data)
                ax2.set_title("Histogram of " + name + " values")
            except TypeError, e:
                print(e)
                ax1 = figure.add_subplot(111)
                ax1.plot(dates, data, 'r.')

            ax1.set_title("Scatterplot of " + name + " values")

        else:
            ax1 = figure.add_subplot(111)
            ax1.set_title("No data to plot")

        # refresh canvas
        plotCanvas.draw()

    @QtCore.pyqtSlot()
    def _reset_generate_time_stats_button(self):
        """
        Rest the generate time stats button to initial state
        :return:
        """
        self.pushButtonGenerateTimeStats.setText("Generate")
        self.pushButtonGenerateTimeStats.setStyleSheet("")
        self.pushButtonGenerateTimeStats.setEnabled(True)
        self.pushButtonAbortTimeSeriesStats.setEnabled(False)

    @QtCore.pyqtSlot()
    def _error_in_time_series_plots(self, e=Exception("Unknown error")):
        QtGui.QMessageBox.critical(self, "Error", str(e))
        self._reset_time_series_plots()

    def _reset_time_series_plots(self):
        """
        Reset all matplotlib figures and canvas to initial state
        :return:
        """
        self.lineEditSourceFile.clear()
        self.tableWidgetRawData.clear()
        self.figureMax.clear()
        self.figureMean.clear()
        self.figureMin.clear()
        self.figureStddev.clear()
        self.figureVarianz.clear()

        self.plotCanvasMax.draw()
        self.plotCanvasMean.draw()
        self.plotCanvasMin.draw()
        self.plotCanvasStddev.draw()
        self.plotCanvasVarianz.draw()

    def _set_generate_time_stats_button_busy(self):
        """
        Mark the generate time series stats as busy
        :return:
        """
        self.pushButtonGenerateTimeStats.setText("Busy")
        self.pushButtonGenerateTimeStats.setStyleSheet("background-color: red")
        self.pushButtonGenerateTimeStats.setEnabled(False)
        self.pushButtonAbortTimeSeriesStats.setEnabled(True)

    @QtCore.pyqtSlot()
    def generate_timeseries_stats_and_plot(self):
        """
        Start the worker thread and use t.rast.univar
        to compute univariate statistics of
        the time series
        """
        # Quit the thread and disconnect any slots
        self.objThread.quit()
        try:
            self.objThread.started.disconnect()
            self.objThread.finished.disconnect()
            self.time_stats.abort.disconnect()
            self.time_stats.error.disconnect()
            self.time_stats.finished.disconnect()
        except:
            pass
        self.objThread.terminate()

        self.objThread = QtCore.QThread()
        self.time_stats = TimeSeriesStatisticsComputation(self.strds, self)

        # Set the button to busy
        self._set_generate_time_stats_button_busy()
        # Set the time series stats values
        self.time_stats.set_values(self.lineEditWhereTimeStats.text(),
                                   self)
        # Move the object to the thread
        self.time_stats.moveToThread(self.objThread)

        self.objThread.started.connect(self.time_stats.compute_stats)
        self.objThread.finished.connect(self._reset_generate_time_stats_button)

        self.time_stats.abort.connect(self._reset_generate_time_stats_button)
        self.time_stats.abort.connect(self.objThread.quit)
        self.time_stats.error.connect(self._error_in_time_series_plots)
        self.time_stats.finished.connect(self.generate_timeseries_plots)
        self.time_stats.finished.connect(self.objThread.quit)

        # Worker thread for time series statistics computation
        ret = self.objThread.start()

    @QtCore.pyqtSlot()
    def kill_time_stats_process(self):
        self.mutex.lock()
        try:
            self.time_stats.t_rast_univar.popen.terminate()
        except Exception, e:
            pass
        finally:
            self.time_stats.abort.emit()
            try:
                self.time_stats.abort.disconnect()
                self.time_stats.finished.disconnect()
                self.objThread.started.disconnect()
                self.objThread.finished.disconnect()
            except:
                pass

            self.mutex.unlock()

    def setup_raster_plot(self):

        # Setup the plot widget
        self.figureRaster = plt.figure()
        self.plotCanvasRaster = FigureCanvas(self.figureRaster)
        self.verticalLayoutRasterStats.addWidget(self.plotCanvasRaster)

    @QtCore.pyqtSlot()
    def generate_raster_history_plot(self):
        """
        Generate the histogram plot of the current selected raster map layer
        :return: 
        """
        if self.check_raster_layer() is True:
            # Generate the data to plot
            self.figureRaster.clear()
            self.raster_ax = self.figureRaster.add_subplot(111)
            self.raster_ax.set_xlim(self.strds.metadata.get_min_min(),
                                    self.strds.metadata.get_max_max())
            self.raster_ax.hold(False)

            raster_id = self.strds_layer.raster_name + "@" + \
                        self.strds_layer.mapset

            temp_file = gscript.tempfile(1)
            nsteps = self.rasterStatsSteps.value()

            # Type of visualization
            text = self.comboBoxRasterStats.currentText()

            flags = "An"

            if text == "Cells":
                flags += "c"
                self.raster_ax.set_ylabel("Number of cells")
            elif text == "Area":
                flags += "a"
                self.raster_ax.set_ylabel("Area")
            elif text == "Percent":
                flags += "p"
                self.raster_ax.set_ylabel("Percent")

            # We need to use a temporary region to compute the statistics
            gscript.use_temp_region()
            if self.checkBoxUseRasterRegion.isChecked() is True:
                gscript.run_command("g.region", rast=raster_id)
            gscript.run_command("r.stats", flags=flags,
                                input=raster_id, sep="|",
                                output=temp_file,
                                overwrite=True,
                                nsteps=nsteps)
            gscript.del_temp_region()

            r = csv.reader(open(temp_file, "r"), delimiter="|")
            x = []
            y = []
            y_min = []

            for e in r:
                x.append(float(e[0].replace("%", "")))
                y.append(float(e[1].replace("%", "")))
                y_min.append(0)

            self.raster_ax.vlines(x, y_min, y, colors="black")

            # refresh canvas
            self.plotCanvasRaster.draw()

    def set_opacity(self):
        self.strds_layer.set_opacity(self.spinBoxOpacity.value())

    def switch_raster_layer(self, item, column):
        """
        Switch the raster map layer that will be visualized in the
        map canvas
        """
        if item is None:
            return
        raster_name = item.text(0)
        self.strds_layer.change_map(raster_name)
        self.rasterNameLabel.setText("Current map:   " + raster_name)

    @QtCore.pyqtSlot()
    def generate_show_map_list(self):
        """
        Read the maps the a space time raster dataset using where and order
        statements and call the method to list them in the map list tree widget.
        :return:
        """

        self.map_list = []
        where = self.lineEditWhere.text()
        order = self.comboBoxOrder.currentText()

        self.pushButtonListMaps.setText("Busy")
        self.pushButtonListMaps.setStyleSheet("background-color: red")

        try:
            dbif, connected = tgis.init_dbif(self.dbif)
            # Generate the maplist
            self.map_list = self.strds.get_registered_maps(columns="name,start"
                                                                   "_time,end_"
                                                                   "time,min,"
                                                                   "max",
                                                           where=where,
                                                           order=order,
                                                           dbif=self.dbif)
            self.show_map_list()
        except Exception, e:
            QtGui.QMessageBox.critical(self, "Error", "Error in SQL:\n%s"%(str(e)))
            return
        finally:

            if connected is True:
                self.dbif.close()

            self.pushButtonListMaps.setText("List maps")
            self.pushButtonListMaps.setStyleSheet("")

    def show_map_list(self):
        """
        Fill the tree widgets with raster maps
        """

        self.map_name_dict.clear()
        self.treeWidget.clear()
        self.treeWidget.setColumnCount(5)
        self.treeWidget.setHeaderLabels(["Map name", "Start time",
                                         "End time", "Min", "Max"])

        if len(self.map_list) == 0:
            return

        count = 0
        for _map in self.map_list:
            map_item = QtGui.QTreeWidgetItem()
            map_item.setText(0, _map["name"])
            map_item.setText(1, str(_map["start_time"]))
            map_item.setText(2, str(_map["end_time"]))
            map_item.setText(3, str(_map["min"]))
            map_item.setText(4, str(_map["max"]))

            # Use the name as key and the item as value
            self.map_name_dict[_map["name"]] = map_item

            self.treeWidget.insertTopLevelItem(count, map_item)
            count += 1

        self.treeWidget.resizeColumnToContents(0)
        self.treeWidget.resizeColumnToContents(1)
        self.treeWidget.resizeColumnToContents(2)
        self.treeWidget.resizeColumnToContents(3)

    def update_map_list_selection(self):
        """
        Select the currently rendered map in the map list
        :return:
        """
        if self.strds_layer.raster_name in self.map_name_dict:
            self.treeWidget.clearSelection()
            item = self.map_name_dict[self.strds_layer.raster_name]
            self.treeWidget.setItemSelected(item, True)

    def apply_colors(self):
        """
        Apply the chosen color
        :return:
        """
        color = self.treeWidgetColors.currentItem().text(0)
        if color:
            gscript.run_command("t.rast.colors", input=self.strds.get_id(), color=color)
            self.iface.mapCanvas().refresh()
            self.strds_layer.repaintRequested.emit()
            self.strds_layer.legendChanged.emit()